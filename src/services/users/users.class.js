// This is the database adapter service class
const { Service } = require('feathers-nedb');

exports.Users = class Users extends Service {
  create (data, params) {
    // This is the information we want from the user signup data
    const { email, password, name } = data;
    // Use the existing avatar image or return the Gravatar for the email
    // The complete user
    const userData = {
      email,
      name,
      password,
    };

    // Call the original `create` method with existing `params` and new data
    return super.create(userData, params);
  }
};
